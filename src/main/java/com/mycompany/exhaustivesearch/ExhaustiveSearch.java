/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.exhaustivesearch;

/**
 *
 * @author Lenovo
 */
public class ExhaustiveSearch {

    public int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;
        
         for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }
    
        return maxProfitSoFar;
    }
}
